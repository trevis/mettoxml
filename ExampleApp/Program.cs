﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleApp {
    class Program {
        static void Main(string[] args) {
            var metFilePath = string.Join(" ", args);

            // create a new converter object, passing in the met file absolute path
            // ie: C:\Games\VirindiPlugins\VirindiTank\ibcontrol.met
            var converter = new MetToXML.Converter(metFilePath);

            // converter.Convert returns bool based on success
            if (!converter.Convert()) {
                Console.WriteLine("Error converting: " + converter.LastError);
                return;
            }

            // convert.XML now holds mimb xml
            Console.WriteLine(converter.XML);
        }
    }
}
