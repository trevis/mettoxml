﻿using MetToXML.MetParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace MetToXML {
    public class Converter {
        public string LastError = "";
        public string XML = "";
        public string MetaFilePath = "";

        public Converter(string metaFilePath) {
            MetaFilePath = metaFilePath;
        }

        public bool Convert() {
            try {
                if (!File.Exists(MetaFilePath)) {
                    LastError = "Unable to find file: " + MetaFilePath;
                    return false;
                }

                var parser = new MetToXML.MetParser.Parser(MetaFilePath);
                var success = parser.Parse();

                if (success) {
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.IndentChars = "\t";
                    MemoryStream memoryStream = new MemoryStream();
                    XmlWriter writer = XmlWriter.Create(memoryStream, settings);

                    writer.WriteStartDocument();
                    writer.WriteStartElement("NewDataSet");
                    for (var i = 0; i < parser.MainTable.RowCount; i++) {
                        var row = parser.MainTable.Rows[i];

                        writer.WriteStartElement("table");

                        var conditionType = ((ConditionType)((MetInteger)row["CType"]).Value).ToString();
                        writer.WriteElementString("Condition_x0020_Type", conditionType);

                        var actionType = ((ActionType)((MetInteger)row["AType"]).Value).ToString();
                        writer.WriteElementString("Action_x0020_Type", actionType);

                        writer.WriteElementString("Condition_x0020_Data", WriteConditionData(row));

                        writer.WriteElementString("Action_x0020_Data", WriteActionData(row));

                        writer.WriteElementString("State", row["State"].ValueAsString());

                        writer.WriteEndElement(); //table
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();

                    writer.Flush();
                    writer.Close();

                    memoryStream.Position = 0;
                    StreamReader reader = new StreamReader(memoryStream);
                    XML = reader.ReadToEnd();

                    return true;
                }
                else {
                    LastError = parser.LastError;
                    return false;
                }
            }
            catch (Exception ex) { LastError = ex.Message; }

            return false;
        }

        private string WriteActionData(MetTable.Row row) {
            var actionType = ((ActionType)((MetInteger)row["AType"]).Value).ToString();
            var actionData = row["AData"].ValueAsString();

            if (!string.IsNullOrEmpty(actionData)) return actionData;

            if ((ActionType)((MetInteger)row["AType"]).Value == ActionType.Multiple) {
                var table = ((MetTable)row["AData"]);
                var outputs = new List<string>();
                for (var i = 0; i < table.RowCount; i++) {
                    var aType = ((ActionType)((MetInteger)table[i][0]).Value).ToString();
                    if (table[i][1].TypeAsString().ToLower() == "table") {
                        var aTable = ((MetTable)table[i][1]);
                        outputs.Add(aType + "{" + aTable.GetValues() + "}");
                    }
                    else {
                        outputs.Add(WriteRowValue(table[i]));
                    }
                }
                return string.Join("", outputs.ToArray());
            }
            else {
                return ((MetTable)row["AData"]).GetValues();
            }
        }

        private string WriteRowValue(MetTable.Row row) {
            var rawValue = row[0].ValueAsString();
            string aType = GetAType(row[0]);

            return aType + "{" + row[1].ValueAsString() + "}";
        }

        private string WriteConditionData(MetTable.Row row) {
            var conditionType = ((ConditionType)((MetInteger)row["CType"]).Value);
            var conditionData = row["CData"].ValueAsString();

            // convert landblock/cells to hex text
            if (conditionType == ConditionType.LandBlockE || conditionType == ConditionType.LandCellE) {
                conditionData = ((MetInteger)row["CData"]).Value.ToString("X8");
            }

            if (!string.IsNullOrEmpty(conditionData)) return conditionData;

            switch ((ConditionType)((MetInteger)row["CType"]).Value) {
                case ConditionType.All:
                    return WriteMultiConditionData(row);
                case ConditionType.Any:
                    return WriteMultiConditionData(row);
                case ConditionType.Not:
                    return WriteMultiConditionData(row);
            }

            return ((MetTable)row["CData"]).GetValues();
        }

        private string WriteMultiConditionData(MetTable.Row row) {
            var table = ((MetTable)row["CData"]);
            var outputs = new List<string>();

            for (var i = 0; i < table.RowCount; i++) {
                var cType = ((ConditionType)((MetInteger)table[i][0]).Value);

                if (cType == ConditionType.All || cType == ConditionType.Any || cType == ConditionType.Not) {
                    var cTable = ((MetTable)table[i][1]);
                    var cTableOutputs = new List<string>();

                    for (var x = 0; x < cTable.RowCount; x++) {
                        cTableOutputs.Add(WriteEmbeddedMultiConditionData(cTable[x]));
                    }

                    outputs.Add(cType.ToString() + "{" + string.Join("", cTableOutputs.ToArray()) + "}");
                }
                else if (table[i][1].TypeAsString().ToLower() == "table") {
                    outputs.Add(cType.ToString() + "{" + ((MetTable)table[i][1]).GetValues() + "}");
                }
                else {
                    var conditionData = table[i][1].ValueAsString();

                    // convert landblock/cells to hex text
                    if (cType == ConditionType.LandBlockE || cType == ConditionType.LandCellE) {
                        conditionData = ((MetInteger)table[i][1]).Value.ToString("X8");
                    }

                    outputs.Add(cType.ToString() + "{" + conditionData + "}");
                }
            }

            return string.Join("", outputs.ToArray());
        }

        private string WriteEmbeddedMultiConditionData(MetTable.Row row) {
            string cType = GetCType(row[0]);

            var outputs = new List<string>();

            if (cType == "All" || cType == "Any" || cType == "Not") {
                var cTable = ((MetTable)row[1]);
                var cTableOutputs = new List<string>();

                for (var x = 0; x < cTable.RowCount; x++) {
                    cTableOutputs.Add(WriteEmbeddedMultiConditionData(cTable[x]));
                }

                outputs.Add(cType + ": {" + string.Join("", cTableOutputs.ToArray()) + "}");
            }
            else if (row[1].TypeAsString().ToLower() == "table") {
                outputs.Add(cType + ": {" + ((MetTable)row[1]).GetValues() + "}");
            }
            else {
                var stringValue = row[1].ValueAsString();
                var c = ((ConditionType)((MetInteger)row[0]).Value);
                // convert landblock/cells to hex text
                if (c == ConditionType.LandBlockE || c == ConditionType.LandCellE) {
                    stringValue = ((MetInteger)row[1]).Value.ToString("X8");
                }
                outputs.Add(cType + ": {" + stringValue + "}");
            }

            return string.Join("", outputs.ToArray());
        }

        private string GetAType(MetDataType data) {
            string aType = ((ActionType)((MetInteger)data).Value).ToString();

            return aType;
        }

        private string GetCType(MetDataType data) {
            string cType = ((ConditionType)((MetInteger)data).Value).ToString();

            return cType;
        }
    }
}
