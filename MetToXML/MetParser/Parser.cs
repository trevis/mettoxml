﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace MetToXML.MetParser {
    class Parser {
        public int Version;
        public string LastError;
        public string FileName;

        public MetTable MainTable;
        
        public Parser(string fileName) {
            FileName = fileName;
        }

        public bool Parse() {
            try {
                if (!File.Exists(FileName)) {
                    LastError = "Could not find meta file: " + FileName;
                    return false;
                }

                using (StreamReader sr = File.OpenText(FileName)) {
                    string line = sr.ReadLine();

                    if (!int.TryParse(line, out Version)) {
                        SetError("Could not read version header: " + line);
                        return false;
                    }

                    string mainTable = sr.ReadLine();

                    MainTable = new MetTable(sr);

                    if (!MainTable.Parse()) {
                        LastError = MainTable.LastError;
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception ex) { LastError = ex.Message; }

            SetError("Could not parse meta file: " + FileName);
            return false;
        }

        internal void ToXML(string outputFile) {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter writer = XmlWriter.Create(outputFile, settings);

            MainTable.WriteXML(writer);

            writer.Flush();
            writer.Close();
        }

        public void Print() {
            Console.WriteLine($"Meta File: {FileName.Split(Path.DirectorySeparatorChar).Last()}");
            Console.WriteLine($"Meta Version: {Version}");

            MainTable.Print();
        }

        private void SetError(string error) {
            LastError = error;
            Console.WriteLine(error);
        }

        public string GetMIMBXML() {
            var output = "";

            for (var i = 0; i < MainTable.RowCount; i++) {
                var conditionType = (ConditionType)((MetInteger)MainTable[i]["CType"]).Value;
                var actionType = (ActionType)((MetInteger)MainTable[i]["AType"]).Value;
                var state = ((MetString)MainTable[i]["State"]).Value;
            }

            return output;
        }
    }
}
