﻿using System;
using System.IO;
using System.Xml;

namespace MetToXML.MetParser {
    internal class MetString : MetDataType {
        public string Value = "";

        private StreamReader sr;

        public MetString(StreamReader sr) {
            this.sr = sr;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("string", Value);
        }

        public override string TypeAsString() {
            return "s";
        }

        public override string ValueAsString() {
            return Value;
        }

        internal override bool Parse() {
            try {
                Value = sr.ReadLine();
                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = "Unable to parse string.";
            return false;
        }
    }
}