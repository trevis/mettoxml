﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace MetToXML.MetParser {
    internal class MetByteArray : MetDataType {
        public string Data = "";

        private static int navFileIndex = 1;

        private StreamReader sr;

        public MetByteArray(StreamReader sr) {
            this.sr = sr;
        }

        public override void Print() {
            Console.Write($"{Data}");
        }

        public override string TypeAsString() {
            return "ba";
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("bytearray", Data);
        }

        public override string ValueAsString() {
            return Data.ToString();
        }

        internal override bool Parse() {
            try {
                int bytesToRead;
                var byteLine = sr.ReadLine();

                if (!int.TryParse(byteLine, out bytesToRead)) {
                    LastError = "Error parsing byteLine for MetByteArray";
                    return false;
                }

                var buffer = new char[bytesToRead];
                sr.Read(buffer, 0, buffer.Length);

                Data = bytesToRead > 0 ? new string(buffer) : "[None]";

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = "Unable to parse string.";
            return false;
        }
    }
}