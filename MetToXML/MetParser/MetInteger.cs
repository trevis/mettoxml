﻿using System;
using System.IO;
using System.Xml;

namespace MetToXML.MetParser {
    internal class MetInteger : MetDataType {
        public int Value = -1;

        private StreamReader sr;

        public MetInteger(StreamReader sr) {
            this.sr = sr;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("integer", Value.ToString());
        }

        public override string TypeAsString() {
            return "i";
        }

        public override string ValueAsString() {
            return Value.ToString();
        }

        internal override bool Parse() {
            try {
                var line = sr.ReadLine();

                if (!int.TryParse(line, out Value)) {
                    LastError = "Unable to parse integer from: " + line;
                    return false;
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = "Unable to parse integer.";
            return false;
        }
    }
}