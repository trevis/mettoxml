﻿using System.Xml;

namespace MetToXML.MetParser {
    internal abstract class MetDataType {
        public string LastError;

        internal abstract bool Parse();
        public abstract void Print();

        public abstract string TypeAsString();
        public abstract string ValueAsString();
        public abstract void WriteXML(XmlWriter writer);
    }
}