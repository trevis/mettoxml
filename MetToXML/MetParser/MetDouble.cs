﻿using System;
using System.IO;
using System.Xml;

namespace MetToXML.MetParser {
    internal class MetDouble : MetDataType {
        public double Value = -1;

        private StreamReader sr;

        public MetDouble(StreamReader sr) {
            this.sr = sr;
        }

        public override void Print() {
            Console.Write($"{Value}");
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteElementString("double", Value.ToString());
        }

        public override string TypeAsString() {
            return "d";
        }

        public override string ValueAsString() {
            return Value.ToString();
        }

        internal override bool Parse() {
            try {
                var line = sr.ReadLine();

                if (!double.TryParse(line, out Value)) {
                    LastError = "Unable to parse double from: " + line;
                    return false;
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = "Unable to parse integer.";
            return false;
        }
    }
}