﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetToXML.MetParser {
    enum ConditionType {
        Never = 0,
        Always,
        All,
        Any,
        ChatMessage,
        MainPackSlotsLE,
        SecondsInStateGE,
        NavrouteEmpty,
        Died,
        VendorOpen,
        VendorClosed,
        ItemCountLE,
        ItemCountGE,
        MonsterCountWithinDistance,
        MonstersWithPriorityWithinDistance,
        NeedToBuff,
        NoMonstersWithinDistance,
        LandBlockE,
        LandCellE,
        PortalspaceEnter,
        PortalspaceExit,
        Not,
        SecondsInStatePersistGE,
        TimeLeftOnSpellGE,
        BurdenPercentGE,
        DistanceToAnyRoutePointGE,
        Expression,
        ClientDialogPopup,
        ChatMessageCapture,
    }
}
