﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace MetToXML.MetParser {
    class MetTable : MetDataType {
        public int ColumnCount = 0;
        public int RowCount = 0;
        public List<string> ColumnNames = new List<string>();
        public List<bool> ColumnIndexing = new List<bool>();
        public List<Row> Rows = new List<Row>();

        StreamReader sr;

        public MetTable(StreamReader sr) {
            this.sr = sr;
        }

        public override void WriteXML(XmlWriter writer) {
            writer.WriteStartElement("table");
            writer.WriteStartElement("cols");
            for (var i = 0; i < ColumnCount; i++) {
                writer.WriteStartElement("col");
                writer.WriteAttributeString("indexed", ColumnIndexing[i].ToString());
                writer.WriteString(ColumnNames[i]);
                writer.WriteEndElement(); // column
            }
            writer.WriteEndElement(); // columns


            writer.WriteStartElement("rows");
            for (var i = 0; i < RowCount; i++) {
                writer.WriteStartElement("row");
                for (var x = 0; x < ColumnCount; x++) {
                    //writer.WriteStartElement("col");
                    this[i][x].WriteXML(writer);
                    //writer.WriteEndElement(); // col
                }
                writer.WriteEndElement(); // row
            }
            writer.WriteEndElement(); // rows


            writer.WriteEndElement(); // table
        }

        public override string TypeAsString() {
            return "TABLE";
        }

        public override string ValueAsString() {
            return "";
        }

        public Row this[int index] {
            get {
                return Rows[index];
            }

            set {
                Rows[index] = value;
            }
        }

        internal override bool Parse() {
            if (!ParseColumnCount()) return false;
            if (!ParseColumnNames()) return false;
            if (!ParseColumnIndexes()) return false;
            if (!ParseBody()) return false;

            return true;
        }

        private bool ParseColumnCount() {
            try {
                var columnCount = sr.ReadLine();

                if (!int.TryParse(columnCount, out ColumnCount)) {
                    LastError = $"Unable to parse ColumnCount for table: {columnCount}";
                    return false;
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = $"Unable to parse ColumnCount for table";
            return false;
        }

        private bool ParseColumnNames() {
            try {
                for (var i = 0; i < ColumnCount; i++) {
                    var columnName = sr.ReadLine();

                    if (String.IsNullOrEmpty(columnName)) {
                        LastError = $"Unable to parse ColumnName at column #{i} in table";
                        return false;
                    }

                    ColumnNames.Add(columnName);
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = $"Unable to parse ColumnNames for table";
            return false;
        }

        private bool ParseColumnIndexes() {
            try {
                for (var i = 0; i < ColumnCount; i++) {
                    bool columnIsIndexed;
                    var line = sr.ReadLine();

                    switch (line) {
                        case "y":
                            columnIsIndexed = true;
                            break;

                        case "n":
                            columnIsIndexed = false;
                            break;

                        default:
                            LastError = $"Unable to read column isIndexed value at column #{i} in table";
                            return false;
                    }

                    ColumnIndexing.Add(columnIsIndexed);
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            LastError = $"Unable to parse isIndexed column values for table";
            return false;
        }

        private bool ParseBody() {
            try {
                var rowCount = sr.ReadLine();

                if (!int.TryParse(rowCount, out RowCount)) {
                    LastError = $"Unable to parse RowCount for table";
                    return false;
                }
                
                for (var i = 0; i < RowCount; i++) {
                    if (!ParseRow()) {
                        if (string.IsNullOrEmpty(LastError)) LastError = $"Unable to parse row #{i} in table";
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            return false;
        }

        private bool ParseRow() {
            try {
                var row = new Row(this);

                for (var i = 0; i < ColumnCount; i++) {
                    var type = sr.ReadLine();

                    MetDataType record = null;

                    switch (type) {
                        case "TABLE": // table obv
                            record = new MetTable(sr);
                            break;

                        case "i": // integer
                            record = new MetInteger(sr);
                            break;

                        case "d": // double
                            record = new MetDouble(sr);
                            break;

                        case "ba": // byte array
                            record = new MetByteArray(sr);
                            break;

                        case "s": // string
                            record = new MetString(sr);
                            break;
                    }

                    if (record == null) {
                        LastError = "Something went wrong parsing child table row, unknown type maybe: " + type;
                        return false;
                    }

                    if (!record.Parse()) {
                        LastError = record.LastError;
                        if (string.IsNullOrEmpty(LastError)) LastError = "Something went wrong parsing child table row.";
                        return false;
                    }

                    row[ColumnNames[i]] = record;
                }

                Rows.Add(row);

                return true;
            }
            catch (Exception ex) { LastError = ex.Message; }

            return false;
        }

        public override void Print() {
            Console.WriteLine($"Table:");
            Console.WriteLine($"\tColumn Count: {ColumnCount}");
            Console.WriteLine($"\tColumn Names: {String.Join(", ", ColumnNames.ToArray())}");
            Console.WriteLine($"\tColumn IsIndexed: {String.Join(", ", ColumnIndexing.Select(i => i ? "yes" : "no").ToArray())}");
            Console.WriteLine(string.Join("\t", ColumnNames.ToArray()));

            for (var i = 0; i < RowCount; i++) {
                this[i].Print();
            }
        }

        public string GetValues() {
            var values = new List<string>();

            for (var i=0; i < RowCount; i++) {
                values.Add(this[i][1].ValueAsString());
            }

            return String.Join(";", values.ToArray());
        }

        public class Row {
            public MetTable ParentTable;
            
            Dictionary<string, MetDataType> Data = new Dictionary<string, MetDataType>();

            public Row(MetTable parentTable) {
                ParentTable = parentTable;
            }

            public MetDataType this[int index] {
                get {
                    var name = index < ParentTable.ColumnCount ? ParentTable.ColumnNames[index] : null;
                    return string.IsNullOrEmpty(name) ? null : this[name];
                }

                set {
                    var name = index < ParentTable.ColumnCount ? ParentTable.ColumnNames[index] : null;
                    this[name] = value;
                }
            }

            public MetDataType this[string columnName] {
                get {
                    return Data[columnName];
                }

                set {
                    Data[columnName] = value;
                }
            }

            public void Print() {
                foreach (var d in Data) {
                    Console.Write($"{d.Value.ValueAsString()}\t");
                }

                Console.Write("\n");
            }
        }
    }
}
