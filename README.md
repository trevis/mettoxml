# MetToXML

This will convert a vtank meta file into a mimb compatible xml file.  It ships with two projects, one is a dll for use in other projects, the other is an example app. The example app will accept a met file path as a command line argument, and write the xml to the console.

## Usage

Check out the [releases](https://gitlab.com/trevis/mettoxml/-/releases) page for the latest binaries. Add the dll as a reference to your project, check out ExampleApp for usage.